# Characters

This is an example of [Phoenix](http://phoenixframework.org/) project. The main purpose of this project is to serve as an example of project with `GitLab CI/CD` and `Docker Compose` by the context of [LAPPIS](https://fga.unb.br/lappis).

## Installation (Production)

* Clone this repo:

```bash
git clone https://gitlab.com/lappis-unb/internal/guides/examples/characters.git
```

* Update the production [environment variables](compose/prod). Don't forget to remove the suffix `.example` from each file;

* Modify the `host` attribute in `url` at [api/config/prod.exs](api/config/prod.exs) as desired;

* Create the production configuration secret file in `api/config/prod.secret.exs`, following the example:

```elixir
use Mix.Config

config :api, ApiWeb.Endpoint,
  secret_key_base: "{KEY_GENERATED}"

config :api, Api.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USER"),
  password: System.get_env("POSTGRES_PASSWORD"),
  database: "api_prod",
  hostname: System.get_env("POSTGRES_HOST"),
  pool_size: 15
```

* Generate the secret key base with the following command and copy it in `{KEY_GENERATED}` at the file from the previous step:

```bash
docker-compose -f docker-compose.prod.yml run api mix phx.gen.secret
```

* Initialize the services with the following command:

```bash
docker-compose -f docker-compose.prod.yml up -d
```

## Usage

A single entity called `Character` is exposed as RESTful API in route `/api/v1/characters`.

An example of Character entity:

```javascript
{
  "id": 1,
  "first_name": "Jon",
  "last_name": "Snow",
  "age": 14,
  "origin": "A Song of Ice and Fire"
}
```

## `GET /api/v1/characters`

* **Description**: Show all characters;
* **cURL sample**:

```bash
curl -X GET -H "Accept: application/json" {host}:{port}/api/v1/characters
```

* **Result sample**:

```json
{
  "data": [
    {
      "id": 1,
      "first_name": "Jon",
      "last_name": "Snow",
      "age": 14,
      "origin": "A Song of Ice and Fire"
    }, {
      "id": 2,
      "first_name": "Walter",
      "last_name": "White",
      "age": 50,
      "origin": "Breaking Bad"
    }, {
      "id": 3,
      "first_name": "Locke",
      "last_name": "Cole",
      "age": 25,
      "origin": "Final Fantasy VI"
    }, {
      "id": 4,
      "first_name": "Arthas",
      "last_name": "Menethil",
      "age": 24,
      "origin": "Warcraft III"
    }, {
      "id": 5,
      "first_name": "Dominick",
      "last_name": "Cobb",
      "age": 37,
      "origin": "Inception"
    }, {
      "id": 6,
      "first_name": "Vincent",
      "last_name": "Vega",
      "age": 27,
      "origin": "Pulp Fiction"
    }
  ]
}
```

## `GET /api/v1/characters/:id`

* **Description**: Show character with id equals to `:id`;
* **cURL sample**:

```bash
curl -X GET -H "Accept: application/json" {host}:{port}/api/v1/characters/{:id}
```

* **Result sample**:

```json
{
  "data": {
    "id": 1,
    "first_name": "Jon",
    "last_name": "Snow",
    "age": 14,
    "origin": "A Song of Ice and Fire"
  }
}
```

## `POST /api/v1/characters`

* **Description**: Create a new character;
* **cURL sample**:

```bash
curl -X POST -H "Accept: application/json" -H "Content-Type: application/json" -d '{"character":{"first_name":"{first_name}","last_name":"{last_name}","age":{age},"origin":"{origin}"}}' {host}:{port}/api/v1/characters
```

* **Input sample**:

```json
{
  "character": {
    "first_name": "Jon",
    "last_name": "Snow",
    "age": 14,
    "origin": "A Song of Ice and Fire"
  }
}
```

* **Result sample**:

```json
{
  "data": {
    "id": 1,
    "first_name": "Jon",
    "last_name": "Snow",
    "age": 14,
    "origin": "A Song of Ice and Fire"
  }
}
```

## `PATCH /api/v1/characters/:id`

* **Description**: Partially updates the character with id equals to `:id`;
* **cURL sample**:

```bash
curl -X PATCH -H "Accept: application/json" -H "Content-Type: application/json" -d '{"character":{"first_name":"{first_name}"}}' {host}:{port}/api/v1/characters/{:id}
```

* **Input sample**:

```json
{
  "character": {
    "last_name": "Stark"
  }
}
```

* **Result sample**:

```json
{
  "data": {
    "id": 1,
    "first_name": "Jon",
    "last_name": "Stark",
    "age": 14,
    "origin": "A Song of Ice and Fire"
  }
}
```

## `PUT /api/v1/characters/:id`

* **Description**: Updates the entire character with id equals to `:id`;
* **cURL sample**:

```bash
curl -X PUT -H "Accept: application/json" -H "Content-Type: application/json" -d '{"character":{"first_name":"{first_name}","last_name":"{last_name}","age":{age},"origin":"{origin}"}}' {host}:{port}/api/v1/characters/{:id}
```

* **Input sample**:

```json
{
  "character": {
    "first_name": "Thomas",
    "last_name": "Anderson",
    "age": 22,
    "origin": "Matrix"
  }
}
```

* **Result sample**:

```json
{
  "data": {
    "id": 1,
    "first_name": "Thomas",
    "last_name": "Anderson",
    "age": 22,
    "origin": "Matrix"
  }
}
```
  }
}

## `DELETE /api/v1/characters/:id`

* **Description**: Delete character with id equals to `:id`;
* **cURL sample**:

```bash
curl -X DELETE -H "Accept: application/json" {host}:{port}/api/v1/characters/{:id}
```
