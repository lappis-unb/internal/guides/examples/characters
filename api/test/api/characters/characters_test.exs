defmodule Api.CharactersTest do
  use Api.DataCase

  alias Api.Characters

  describe "characters" do
    alias Api.Characters.Character

    @valid_attrs %{age: 42, first_name: "some first_name", last_name: "some last_name", origin: "some origin"}
    @update_attrs %{age: 43, first_name: "some updated first_name", last_name: "some updated last_name", origin: "some updated origin"}
    @invalid_attrs %{age: nil, first_name: nil, last_name: nil, origin: nil}

    def character_fixture(attrs \\ %{}) do
      {:ok, character} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Characters.create_character()

      character
    end

    test "list_characters/0 returns all characters" do
      character = character_fixture()
      assert Characters.list_characters() == [character]
    end

    test "get_character!/1 returns the character with given id" do
      character = character_fixture()
      assert Characters.get_character!(character.id) == character
    end

    test "create_character/1 with valid data creates a character" do
      assert {:ok, %Character{} = character} = Characters.create_character(@valid_attrs)
      assert character.age == 42
      assert character.first_name == "some first_name"
      assert character.last_name == "some last_name"
      assert character.origin == "some origin"
    end

    test "create_character/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Characters.create_character(@invalid_attrs)
    end

    test "update_character/2 with valid data updates the character" do
      character = character_fixture()
      assert {:ok, character} = Characters.update_character(character, @update_attrs)
      assert %Character{} = character
      assert character.age == 43
      assert character.first_name == "some updated first_name"
      assert character.last_name == "some updated last_name"
      assert character.origin == "some updated origin"
    end

    test "update_character/2 with invalid data returns error changeset" do
      character = character_fixture()
      assert {:error, %Ecto.Changeset{}} = Characters.update_character(character, @invalid_attrs)
      assert character == Characters.get_character!(character.id)
    end

    test "delete_character/1 deletes the character" do
      character = character_fixture()
      assert {:ok, %Character{}} = Characters.delete_character(character)
      assert_raise Ecto.NoResultsError, fn -> Characters.get_character!(character.id) end
    end

    test "change_character/1 returns a character changeset" do
      character = character_fixture()
      assert %Ecto.Changeset{} = Characters.change_character(character)
    end
  end
end
