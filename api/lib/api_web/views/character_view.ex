defmodule ApiWeb.CharacterView do
  use ApiWeb, :view
  alias ApiWeb.CharacterView

  def render("index.json", %{characters: characters}) do
    %{data: render_many(characters, CharacterView, "character.json")}
  end

  def render("show.json", %{character: character}) do
    %{data: render_one(character, CharacterView, "character.json")}
  end

  def render("character.json", %{character: character}) do
    %{id: character.id,
      first_name: character.first_name,
      last_name: character.last_name,
      age: character.age,
      origin: character.origin}
  end
end
