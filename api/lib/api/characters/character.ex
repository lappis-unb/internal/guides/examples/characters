defmodule Api.Characters.Character do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Characters.Character


  schema "characters" do
    field :age, :integer
    field :first_name, :string
    field :last_name, :string
    field :origin, :string

    timestamps()
  end

  @doc false
  def changeset(%Character{} = character, attrs) do
    character
    |> cast(attrs, [:first_name, :last_name, :age, :origin])
    |> validate_required([:first_name, :last_name, :age, :origin])
  end
end
