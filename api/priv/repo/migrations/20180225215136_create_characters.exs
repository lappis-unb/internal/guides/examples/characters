defmodule Api.Repo.Migrations.CreateCharacters do
  use Ecto.Migration

  def change do
    create table(:characters) do
      add :first_name, :string
      add :last_name, :string
      add :age, :integer
      add :origin, :string

      timestamps()
    end

  end
end
