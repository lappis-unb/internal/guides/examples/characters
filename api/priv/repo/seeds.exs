# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Api.Repo.insert!(%Api.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Api.Repo
alias Api.Characters.Character

characters = [
  %Character{
    first_name: "Jon",
    last_name: "Snow",
    age: 14,
    origin: "A Song of Ice and Fire"
  },
  %Character{
    first_name: "Walter",
    last_name: "White",
    age: 50,
    origin: "Breaking Bad"
  },
  %Character{
    first_name: "Locke",
    last_name: "Cole",
    age: 25,
    origin: "Final Fantasy VI"
  },
  %Character{
    first_name: "Arthas",
    last_name: "Menethil",
    age: 24,
    origin: "Warcraft III"
  },
  %Character{
    first_name: "Dominick",
    last_name: "Cobb",
    age: 37,
    origin: "Inception"
  },
  %Character{
    first_name: "Vincent",
    last_name: "Vega",
    age: 27,
    origin: "Pulp Fiction"
  }
]

for character <- characters do
  Repo.insert! character
end
